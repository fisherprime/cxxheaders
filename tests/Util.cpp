/**
 * @file DedupVector.cpp
 * @brief Util functions test
 * @author fisherprime
 * @version 0.0.1
 * @date 2019-09-02
 */

#include <vector>
#include <cstdint>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>

#include "util.hpp"

std::vector<int32_t> VecTest(std::vector<int32_t> vec)
{
	DedupVector(vec);

	return vec;
}

TEST_CASE("testing std::vector<int32_t> deduplication function")
{
	std::vector<int32_t> Vector1 = { 7, 6, 6, 2, 4, 7, 5 };
	std::vector<int32_t> Vector2 = { 2, 6, 4, 7 };

	std::vector<int32_t> Dedup1 = { 2, 4, 5, 6, 7 };
	std::vector<int32_t> Dedup2 = { 2, 4, 6, 7 };

	SUBCASE("dedupe vector1")
	{
		CHECK(VecTest(Vector1) == Dedup1);
	}

	SUBCASE("dedup vector2")
	{
		CHECK(VecTest(Vector2) == Dedup2);
	}
}
