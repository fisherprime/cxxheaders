/**
 * @file opengl_shaders.hpp
 * @brief C++ include file, OpenGL shader stuff
 * @author fisherprime
 * @version 1.0.1
 * @date 2019-09-18
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef _OPENGL_SHADERS_HPP
#define _OPENGL_SHADERS_HPP

// Necessary headers
// ////////////////////////////////
// Include additional OpenGl stuff
#include "opengl.hpp"
// ////////////////////////////////

#endif // _OPENGL_SHADERS_HPP
