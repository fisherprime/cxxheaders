#include <vector>
#include <iostream>
#include <limits>
#include <unordered_set>
#include <cstdint>
#include <algorithm>

#include "util.hpp"

extern "C" {
#include "util.h"
}

/**
 * @brief      Clears the input buffer.
 */
inline void ClearInput()
{
	std::cin.clear();
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
} // End ClearInput

/**
 * @brief      Removes duplicate entries from integer vectors
 *
 * @param      InputVector  The input vector
 *
 * @return     state on exit
 */
int8_t DedupVector(std::vector<int32_t> &InputVector)
{
	if (InputVector.size() >= VECTOR_DEDUP_ALGO_CHANGE_LIMIT) {
		/**
		 * @note Using an unordered set is faster, a regular set will sort while inserting.
		 */
		std::unordered_set<int> USet;

		for (int num : InputVector)
			USet.insert(num);

		InputVector.assign(USet.begin(), USet.end());
		sort(InputVector.begin(), InputVector.end());

		return SUCCESS;
	}

	sort(InputVector.begin(), InputVector.end());
	InputVector.erase(unique(InputVector.begin(), InputVector.end()),
			  InputVector.end());

	// Manual conversion to a set
	// set<int> s;
	// unsigned size = vec.size();
	// for( unsigned i = 0; i < size; ++i ) s.insert( vec[i] );
	// vec.assign( s.begin(), s.end() );

	// Conversion to a set using a constructor
	// set<int> s( vec.begin(), vec.end() );
	// vec.assign( s.begin(), s.end() );

	return SUCCESS;
} // End DedupVector
