# CXXHeaders

My CPP library

## Requirements

1. [conan](https://conan.io/)
1. [ninja](https://ninja-build.org/)
1. make

## Building

### Dependency gathering

```sh
conan install .
```

### Building

```sh
conan build .
```

Build and package in one step

```sh
conan package .
```

## Notes

The project is configured to automatically generate the `compile_commands.json` file in the
functions.cmake file.
One can also execute the below code with the `compile_commands.json` export option:

```sh
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=YES .
```

Ninja doesn't have a test target like make, instead run:

```sh
ctest
```
